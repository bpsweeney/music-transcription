import React, {useState, useEffect} from 'react';
import './CSS/style.css';
import './CSS/Home.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';
import { useParams } from 'react-router-dom';

// Would rather use a class component but react-router-dom v6
// only allows for useParams in a functional component from what I can tell now.

function Sheet(){
    let {id} =  useParams()
    const [APDF, setPdf] = useState({ path: ''})
    const [GotError, setErr] = useState({Err: ''})
    let error = "File could not be found. Try refreshing or uploading the wav again."
    //Make code here to make a call to the backend with the SheetId.
    useEffect(() => {
        fetch('/getPDF/' + id, {
            method: 'GET',
            //headers : { 
            //    'Content-Type': 'application/pdf',
            //    'Accept': 'application/pdf'
            //}
        }).then(response => response.json())
        .then(message => setPdf({path:'http://127.0.0.1:5000/putPDFonpage' + message}))
        .catch((error) =>{
            console.log(error)
            setErr({Err: 'T'})
            const Err = document.getElementById("Error");
            Err.style.opacity = 1;
        });
    }, [id]);    

    ////////////////////////////////////////////////////////////////
    return(
        <div>
        <section>
            <Nav />
        </section>
        {(() => {if(GotError.Err == 'T') {
			return (
                <div className='container'>
                <div className="error BIG center-text" id="Error">
                    {error}
                </div>
                </div>
			)
		  }else{
              return(
                <>
                  <div className='container'>
                          <object className="marg-top" title="ThePDF" data={APDF.path} type="application/pdf" width="100%" height="900">
                              {/*<p>Here is a link to the pdf <a href={APDF.path}>to the PDF!</a></p>*/}
                          </object>
                  </div>
                </>
              )
          }
        })()}    
        <Foot />    
        </div>
    )
}

export default Sheet;