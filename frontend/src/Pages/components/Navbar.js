import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/nav.css';
import '../CSS/style.css';
import image from './piano.png';

// Both link to the same place.

class Navbar extends Component {
    render(){
        return(
            <div className="container extra-container-bg">
                <nav className = 'navbar-holder nav-negative-box'>
                    <div className = 'nav-list leading-items'>
                    <div>
                        <Link to='/'>
                            <a href>
                                <img height = '32' src = {image} alt='The thing'/>
                            </a>
                        </Link>
                    </div>
                    </div>

                    <div className = 'nav-list trailing-items'>
                        <div className="nav-link">
                        <Link to='/'>
                            <a href>
                                Home
                            </a>
                        </Link>
                        </div>
                    </div>

                </nav> 
			</div>
        )
    }
}

export default Navbar;