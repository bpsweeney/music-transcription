import React, { Component } from 'react';
import '../CSS/nav.css';
import '../CSS/style.css';

class Footer extends Component {
    render(){
        var u = "Check out the code on: "
        return(
            <div className="container">
                <footer className="center-text foot">
                    {u} 
                    <a href = "https://gitlab.com/bpsweeney/music-transcription">
                        Gitlab
                    </a>
                </footer>
            </div>
        )
    }
}

export default Footer;