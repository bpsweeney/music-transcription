import React, { Component } from 'react';

import AudioReactRecorder, { RecordState } from 'audio-react-recorder';

//////////////////////////////////////////////////////
// Directly from the documentation
// https://www.npmjs.com/package/audio-react-recorder
class Recorder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      recordState: null,
      audioData: null
    }
  }

  start = () => {
    this.setState({
      recordState: RecordState.START
    })
  }

  pause = () => {
    this.setState({
      recordState: RecordState.PAUSE
    })
  }

  stop = () => {
    this.setState({
      recordState: RecordState.STOP
    })
  }

  onStop = (data) => {
    this.setState({
      audioData: data
    })
  }

  render() {
    const { recordState } = this.state

    return (
      <div>
        <div className='row'>
        <div className='col'>
        <AudioReactRecorder
          state={recordState}
          onStop={this.onStop}
          canvasWidth= '150'
          canvasHeight = '100'
        />
        </div>
        <div className='col'>
            <div className='small-label-text'>
                To save the wav click the triple dot and download.
            </div>
        <audio
          id='audio'
          controls
          src={this.state.audioData ? this.state.audioData.url : null}
        ></audio>
        </div>
        </div>
        <br />
        <button className = 'btn btn-red btn-small-change'
        id='record' onClick={this.start}>
          ⬤ Record
        </button>
        <button className = 'btn btn-general btn-small-change'
        id='pause' onClick={this.pause}>
          ll Pause
        </button>
        <button className = 'btn btn-green btn-small-change'
        id='stop' onClick={this.stop}>
          Finalize
        </button>
      </div>
    )
  }
}

export default Recorder;