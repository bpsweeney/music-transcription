import React, { Component } from 'react';
import './CSS/Home.css';
import './CSS/style.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';
import Record from './components/recorder';

class Homepage extends Component{
    constructor(props){
		super(props);
		this.state = {
			file: null,
            btnDisabled: false
		};
	}
    inputchange = (e) => {
        this.setState({ file: e.target.files[0] }, () => {
            console.log(this.state.file);
        });
    }

    render(){
        let val = "Hello world";
        let error = "Error with current file.";
        let process = "The program is currently processing your wav file.";
        let FatalErr = "The backend is down or your wav could not be processed by the program.";
        let disable = false
        if(this.state.file != val){
            val = this.state.file;
        }
        function sendToBack(e){
            e.preventDefault();
            if(disable){
                console.log("Last request is currently in process.")
                return;
            }
            disable = true
            const form = document.getElementById("musicForm");
            //const extra = document.getElementById("extraForm");
            const tempo = form.tempoBPM.value;
            const title = form.title.value;
            const composer = form.composer.value;
            //const FastPeg = extra.Peg.value;
            const Err = document.getElementById("Error");
            const Processing = document.getElementById("Process");
            const Fatal = document.getElementById("BadError");
            Fatal.style.opacity = 0;
            if(val == null){
                Err.style.opacity = 1;
                disable = false;
            }
            else if(val.type != "audio/wav"){
                Err.style.opacity = 1;
                disable = false;
            }
            else{
                Err.style.opacity = 0;
                Processing.style.opacity = 1;
                var data = new FormData();
                data.append("wavfile", val)
                data.append("title", title)
                data.append("composer", composer)
                data.append("BPM", tempo)
                //data.append("FastestPeg", FastPeg)
                fetch('/postWav', {
                    method: 'POST',
                    body: data
                }).then((response) => {
                    if(response.ok){
                        return response.json();
                    }
                    throw new Error("Error");
                }).then(message => {redir(message)})
                .catch((error) => {
                    console.log(error)
                    Processing.style.opacity = 0;
                    Fatal.style.opacity = 1;
                    disable = false;
                });
            }
        }
        function redir(data){
            if(data['1'] == 'Good'){
                let Page = "/sheet/" + data['2'];
                window.location.href = Page;
            }
            else{
                console.log("Error with backend")
                disable = false;
            }
        }
        return(
            <div>
            <section>
			<Nav />
		    </section>
            <div className='container'>
                <div className='bar'>
                    <h1 className='h3 marginb3'>
                        Welcome to the piano transcription website
                    </h1>
                </div>
                <div className="negative-box marginb3">
                    If you want to get started, scroll to the bottom of this page.
                </div>
                <div className="bar">
                    <h1 className="h5 marginb3">
                        How the program works
                    </h1>
                </div>
                <div className="negative-box marginb3">
                    Not sure what else to say right now, but this is open for expansion.
                </div>


            </div>
            {/*Container for making sheet*/}
            <div className = 'container'>    
                <div className = 'info-container info-box'>
                    <div className="HeaderBox">
                        <div className="top-left-ornament"></div>
                        <div className="item">
                            <h1 className="h4">
                                Create your own sheet 
                            </h1>
                        </div>
                    </div>
                    <div className="input-body">
                        <div className="row">
                            <div className="col">
                                {/* Can flip the two stacks.*/}
                                <div className="stack">
                                    <div className="stack-header">
                                        Optional arguments
                                    </div>
                                    <div className="stack-first-mem stack-mem">
                                        <form id="musicForm" className="information" >
                                            <input type="text" className="inputField" name="title" placeholder="Title"
                                            />
                                            <input type="text" className="inputField" name="composer" placeholder="Composer"
                                            />
                                            <input type="number" className="inputField" name="tempoBPM" placeholder="Tempo / BPM" 
                                            />
                                        </form>
                                    </div>
                                    <div className="stack-mem stack-last">
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="stack stack-wav">
                                    <div className="stack-header">
                                        Upload .wav file (Needed)
                                    </div>
                                    <div className="stack-first-mem stack-mem">
                                        <form id="fileForm" className="information">
                                        <div className="">
                                            <label> Upload Your File </label>
                                            <br />
                                            <input type="file" onChange={this.inputchange} name="wav" id="wav"
                                            accept="audio/wav"  required/>
                                        </div>
                                        </form>
                                    </div>
                                    <div className="stack-mem stack-last">
                                    </div>
                                </div>
                                <div className="stack stack-extra">
                                    <div className="stack-header">
                                        Create a wav on this page.
                                    </div>
                                    <div className="stack-first-mem stack-mem">
                                        <Record/>
                                        {/*<form id="extraForm" className="information">
                                            <input type="number" className="inputField" name="Peg" placeholder="Fastest arpeggio"
                                            />
                                           </form>*/}
                                    </div>
                                    <div className="stack-mem stack-last">
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div className="error" id="Error">
                        {error}
                    </div>
                    <div className="Process" id = "Process">
                        {process}    
                    </div>
                    <div className="BadError" id = "BadError">
                        {FatalErr}    
                    </div>
                    <button type="submit" className="btn btn-general btn-small" onClick={sendToBack}>
                        Submit
                    </button>
                </div>

            </div>
            <Foot />
            </div>
        )
    }
}

export default Homepage;