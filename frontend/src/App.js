import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Home from './Pages/Homepage';
import Sheet from './Pages/Sheet';
import './App.css';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route exact path ='/sheet/:id' element={<Sheet />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
