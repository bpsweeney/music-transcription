# Program 2

Program 2 starts by taking inputs for bpm and peg / fastest note and calculating how much to sample per frame it will take.
Check the last call to createSheet() in AbjadTests.py.  Only CreateFreqArrayOnlyScipyV2.py and FreqProcess.py are used outside of AbjadTests.py.

## Section 1
The first section of the code trys to remove as much blank noise from the start of the file.\
It attempts to accomplish this by taking the window size and removing it if the highest amplitude is too low or the note it produces is not legal.\
It does this until no remove is made.  Once it doesn't remove it subtracts 500 from its window size and goes until being less than or equal to 500.

## Section 2
This part takes window length frames until the end of the wav.  From those frames we get a listing of possible notes that are > 200 amplitude.\
If no notes have a amplitude > 200 we take the greatest note. (note because of abs() we skip all even values)

## Section 3
This section separates notes out into left and right hand outputs.  The program favors left hand notes as it seemed that lower frequency notes with larger wavelengths also have lower amplitude outputs.\
For this reason left hand notes will have an easier time being added to the output.  If a left hand note has the highest amplitude then everything has equal power.

## Section 4
This final section sets up the output with different paced notes for the left and right hand separately.  Based on how small the windows are the fade tolerance or chance for holds will change.  This is created at the start before section 1.\
The fade code is only made to work for 4 windows.  So if you have 4ths then it goes 4, 2, 2., then 1.  Then 8ths goes 8, 4, 4., 2. Then 16ths go 16, 8, 8., 4.


## After the ScipyV2 code

It sends the two lists to FreqProcess and gets strings to send to abjad.