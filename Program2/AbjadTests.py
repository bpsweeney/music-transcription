import abjad
#from CreateFreqArray2 import createFreqArray2
from CreateFreqArrayOnlyScipyV2 import createFreqArraySci
from FreqProcess import analyzeFrequenciesSciV2
import re
import os

def createSheet(wavFile, Title = "No Title", composer = "", subtitle = "", bpm=65, peg=4):
    actualWav = 'Backend/Recordings/' + wavFile
    #out = createFreqArray2(actualWav, bpm, peg)
    rout, lout = createFreqArraySci(actualWav, bpm, peg)
    stringO = analyzeFrequenciesSciV2(rout, peg)
    secondOutFromAF = analyzeFrequenciesSciV2(lout, peg)
    #stringO, secondOutFromAF = analyzeFrequencies(out, peg)
    #print(secondOutFromAF)
    #stringO = "r1 r1 r1 r1 r1 r1 r1 r1 r1 r1 r1 r1"
    #secondOutFromAF = "c4 c4 g4 g4 a4 a4 g2 f4 f4 e4 e4 d4 d4 c2 g4 g4 f4 f4 e4 e4 d2 g4 g4 f4 f4 e4 e4 d2 c4 c4 g4 g4 a4 a4 g2 f4 f4 e4 e4 d4 d4 c2"
    #secondOutFromAF = "c'8 f' <g'' c'' f'' e''> d' g' a' b' e'2 a' b' c'' f' b' c''8 c' c'"
                                #"c'16 f' g' a' d' g' a' b' e' a' b' c'' f' b' c'' c' "
    ### 
    voice = abjad.Voice(stringO, name="R_Voice")
    r_staff = abjad.Staff([voice], name="R_Staff")
    lh_voice = abjad.Voice(secondOutFromAF,name="LH_Voice")
    lh_staff = abjad.Staff([lh_voice], name="LH_Staff")
    staff_group = abjad.StaffGroup(
        [r_staff, lh_staff],
        lilypond_type="PianoStaff",
        name="Piano_Staff",
    )
    leaf = abjad.select(staff_group["LH_Voice"]).leaf(0)
    clef = abjad.Clef("bass")
    abjad.attach(clef, leaf)
    score = abjad.Score([staff_group], name="Score")
    string = abjad.lilypond(score)
    #print(string)
    #print(len(string))
    #print(string[118:120])
    ##print(string)
    titleString = " title = \markup {" + Title + "}"
    testString1 = """#(set-global-staff-size 14)
        \header {
            composer = \markup {""" + composer +"""}
            subtitle = \markup {""" + subtitle + """}
        """
    testString2 = """
    }
    \layout {
        indent = 0
    }

    """
    testString = testString1 + titleString + testString2
    m = re.sub(r's', 'is', string)
    mstring = m
    mstring = re.sub(r"baisis","bass", mstring)
    if(bpm != None):
        Bps = str(bpm)
        temp = mstring[:175] + "\t\t\t\t\\tempo 4 = " + Bps + "\n"
        mstring = temp + mstring[175:]
    File = abjad.LilyPondFile([testString, mstring])
    #out = abjad.lilypond(File)
    #print(out)
    title = re.sub(r'\s+', '', Title)
    returnTitle = '/' + title + '.pdf'
    abjad.persist.as_pdf(File, os.path.join(os.getcwd(), 'Backend\\pdfFiles\\' + title))
    return returnTitle


if __name__ == "__main__":
    #createSheet("SilverGarden.wav", "In a Silver Garden with You", )
    #createSheet("Twinkle.wav","Twinkle Twinkle Little Star", bpm=60)
    #createSheet("SWSAS.wav", "RESTTEST", bpm = 184, peg = 16)
    #createSheet("SP65.wav", "NOOOO", bpm = 65, peg = 16)
    #createSheet("Serious2.wav", "ugh")
    createSheet("help.wav", "FE4 1st gen ending","Composer: Yuka Tsujiyoko", "Fire Emblem genealogy of the holy war" ,bpm = 180, peg = 4)
