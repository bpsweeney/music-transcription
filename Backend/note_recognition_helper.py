import array
from collections import Counter

import numpy as np
import scipy
from pydub.utils import get_array_type
from Levenshtein import distance

#TODO -- have to add the rest of the notes
NOTES = {
  ### C0 - B0
    "a,,,": 27.50000,
    "as,,,": 29.13524,
    "b,,,": 30.86771,
  ### C1 - B1
    "c,,": 32.70320,
    "cs,,": 34.64783,
    "d,,": 36.70810,
    "ds,,": 38.89087,
    "e,,": 41.20344,
    "f,,": 43.65353,
    "fs,,": 46.24930,
    "g,,": 48.99943,
    "gs,,": 51.91309,
    "a,,": 55.0000,
    "as,,": 58.2705,
    "b,,": 61.7354,
  ### C2 - B2
    "c,": 65.4064,
    "cs,": 69.2957,
    "d,": 73.4162,
    "ds,": 77.7817,
    "e,": 82.4069,
    "f,": 87.3071,
    "fs,": 92.4986,
    "g,": 97.9989,
    "gs,": 103.826,
    "a,": 110.000,
    "as,": 116.541,
    "b,": 123.471,
  ### C3 - B3
    "c ": 130.813,
    "cs ":  138.591,
    "d ": 146.832,
    "ds ": 155.563,
    "e ": 164.814,
    "f ": 174.614,
    "fs ": 184.997,
    "g ": 195.998,
    "gs ": 207.652,
    "a ": 220.000,
    "as ": 233.082,
    "b ": 246.942,
  ### C4 - B4
    "c'": 261.626,
    "cs'": 277.183,
    "d'": 293.665,
    "ds'": 311.127,
    "e'": 329.628,
    "f'": 349.228,
    "fs'": 369.994,
    "g'": 391.995,
    "gs'": 415.305,
    "a'": 440,
    "as'": 466.1637615180899,
    "b'": 493.8833012561241,
    ### C5 - B5
    "c''": 523.2511306011972,
    "cs''": 554.3652619537442,
    "d''": 587.3295358348151,
    "ds''": 622.2539674441618,
    "e''": 659.2551138257398,
    "f''": 698.4564628660078,
    "fs''": 739.9888454232688,
    "g''": 783.9908719634985,
    "gs''": 830.6093951598903,
    "a''": 880.000,
    "as''": 932.328,
    "b''": 987.767,
  ### C6 - B6
    "c'''": 1046.50,
    "cs'''": 1108.73,
    "d'''": 1174.66,
    "ds'''": 1244.51,
    "e'''": 1318.51,
    "f'''": 1396.91,
    "fs'''": 1479.98,
    "g'''": 1567.98,
    "gs'''": 1661.22,
    "a'''": 1760.00,
    "as'''": 1864.66,
    "b'''": 1975.53,
  ### C7 - B7
    "c''''": 2093.00,
    "cs''''": 2217.46,
    "d''''": 2349.32,
    "ds''''": 2489.02,
    "e''''": 2637.02,
    "f''''": 2793.83,
    "fs''''": 2959.96,
    "g''''": 3135.96,
    "gs''''": 3322.44,
    "a''''": 3520.00,
    "as''''": 3729.31,
    "b''''": 3951.07,
}

def leftAndRightHand(notes):
  lh_notes = []
  rh_notes = []
  order = 0
  for i in notes:
    if((i[-1] == "," and i[-2] == "," and i[-3] == ",")
    or (i[-1] == "," and i[-2] == "," and i[-3] != ",")
    or (i[-1] == "," and i[-2] != ",")
    or (i[-1] == " ")):
      lh_notes.append(i)
      lh_notes.append(order)
      order += 1
    elif(i[-1] == "4"):
      if(i[-3] == " " or i[-3] == "," or i[-4] == "," or i[-5] == ","):
        lh_notes.append(i)
        lh_notes.append(order)
        order += 1
      else:
        rh_notes.append(i)
        rh_notes.append(order)
        order += 1
    else:
      rh_notes.append(i)
      rh_notes.append(order)
      order += 1
  return rh_notes, lh_notes

def frequency_spectrum(sample, max_frequency=4000):
    """
    Derive frequency spectrum of a signal pydub.AudioSample
    Returns an array of frequencies and an array of how prevelant that frequency is in the sample
    """
    # Convert pydub.AudioSample to raw audio data
    # where i found an example https://stackoverflow.com/questions/32373996/pydub-raw-audio-data
    bit_depth = sample.sample_width * 8
    array_type = get_array_type(bit_depth)
    raw_audio_data = array.array(array_type, sample._data)
    n = len(raw_audio_data)

    # Compute FFT and frequency value for each index in FFT array
    # found on https://stackoverflow.com/questions/53308674/audio-frequencies-in-python
    freq_array = np.arange(n) * (float(sample.frame_rate) / n)  # two sides frequency range
    freq_array = freq_array[: (n // 2)]  # one side frequency range

    raw_audio_data = raw_audio_data - np.average(raw_audio_data)  # zero-centering
    freq_magnitude = scipy.fft.fft(raw_audio_data)  # fft computing and normalization
    freq_magnitude = freq_magnitude[: (n // 2)]  # one side

    if max_frequency:
        max_index = int(max_frequency * n / sample.frame_rate) + 1
        freq_array = freq_array[:max_index]
        freq_magnitude = freq_magnitude[:max_index]

    freq_magnitude = abs(freq_magnitude)
    freq_magnitude = freq_magnitude / np.sum(freq_magnitude)
    return freq_array, freq_magnitude


def classify_note_attempt_1(freq_array, freq_magnitude):
    i = np.argmax(freq_magnitude)
    f = freq_array[i]
    print("frequency {}".format(f))
    print("magnitude {}".format(freq_magnitude[i]))
    return get_note_for_freq(f)


def classify_note_attempt_2(freq_array, freq_magnitude):
    note_counter = Counter()
    for i in range(len(freq_magnitude)):
        if freq_magnitude[i] < 0.01:
            continue
        note = get_note_for_freq(freq_array[i])
        if note:
            note_counter[note] += freq_magnitude[i]
    return note_counter.most_common(1)[0][0]

def make_ups(note_counter):
    one, two, three, four = note_counter.most_common(4)
    if (one[0] == "a,," and two[0] == "cs," and three[0] == "gs'"):
      return "a,,,"
    elif (one[0] == "a'" and two[0] == "d," and three[0] == 'a ' and four[0] == 'd '):
      return 'a '
    elif (one[0] == "d," and two[0] == "as,," and three[0] == "a'"):
      return "as,,,"
    elif (one[0] == "fs,," and two[0] == "b,," and three[0] == "a,"):
      return "b,,,"
    elif (one[0] == "g," and two[0] == "c,," and three[0] == "e'"):
      return "c,,"
    elif (one[0] == "gs," and two[0] == "cs,," and three[0] == "f'"):
      return "cs,,"
    elif (one[0] == "a," and two[0] == "d,," and three[0] == "a,,"):
      return "a,,"
    elif (one[0] == "as," and two[0] == "ds,," and three[0] == "as,,"):
      return "as,,"
    elif (one[0] == "b," and two[0] == "b,," and three[0] == "e,,"):
      return "b,,"
    elif (one[0] == "c " and two[0] == "f,," and three[0] == "gs,," and four[0] == "c,"):
      return "c,"
    elif (one[0] == "cs " and two[0] == "fs,," and three[0] == "a,,,"):
      return "cs,"
    elif (one[0] == "d " and two[0] == "g,," and three[0] == "as,,," and four[0] == "a "):
      return "d,"
    # elif (one[0] == "ds " and two[0] == "gs,," and three[0] == "b,,,"):
    #   return "ds,"
    elif (one[0] == "e " and two[0] == "a,," and three[0] == "a,,," and four[0] == "c,,"):
      return "e,"
    elif (one[0] == "f'" and two[0] == "as,," and three[0] == "as," and four[0] == "f "):
      return "f "
    elif (one[0] == "fs'" and two[0] == "b,," and three[0] == "b," and four[0] == "fs "):
      return "fs "
    elif (one[0] == "g'" and two[0] == "c " and three[0] == "c," and four[0] == "ds," and note_counter.most_common(5)[4][0] == "g "):
      return "g "
    elif (one[0] == "f " and two[0] == "f," and three[0] == "gs," and four[0] == "c'''"):
      return "c''"
    elif (one[0] == "g'" and two[0] == 'c,' and three[0] == 'c ' and four[0] == 'g ' and note_counter.most_common(5)[4][0] == 'ds,'):
      return "g "
    #DELETE MAYBE
    elif (one[0] == 'as,' and two[0] == "f'" and three[0] == "f''" and four[0] == 'as,,' and note_counter.most_common(5)[4][0] == 'cs,'):
      return "f'"
    elif (one[0] == "g'" and two[0] == 'c ' and three[0] == 'c,' and four[0] == 'g ' and note_counter.most_common(5)[4][0] == 'ds,'):
      return 'g '

    else:
      return note_counter.most_common(1)[0][0]

def classify_note_attempt_3(freq_array, freq_magnitude):
    # min_freq = 82
    min_freq = 26
    note_counter = Counter()
    for i in range(len(freq_magnitude)-1):
        if freq_magnitude[i] < 0.01:
            continue

        for freq_multiplier, credit_multiplier in [
            (1, 1),
            (1 / 3, 3 / 4),
            (1 / 5, 1 / 2),
            (1 / 6, 1 / 2),
            (1 / 7, 1 / 2),
        ]:
            freq = freq_array[i] * freq_multiplier
            if freq < min_freq:
                continue
            note = get_note_for_freq(freq)
            if note:
                note_counter[note] += freq_magnitude[i] * credit_multiplier
    #new
    check = note_counter.most_common(1)[0][1]
    if(len(note_counter) < 4 and len(note_counter) != 0):
      if(len(note_counter)==3):
        one, two, three = note_counter.most_common(3)
        if(one[0] == "fs,," and two[0] == "b,," and three[0] == "a,"):
          return "b,,,"
      return note_counter.most_common(1)[0][0]
    #what we need to cover the last note...
    elif(len(note_counter) == 0):
      return "b''''"
    # Chords
    elif(len(note_counter) >= 8 and len(freq_magnitude) == 4001 and note_counter.most_common(1)[0][1] < 0.15 and freq_magnitude[0]*100000000000000000 > 0.06):
      one, two, three, four, five = note_counter.most_common(5)
      if (one[0] == "c'" and two[0] == 'f,' and three[0] == 'gs,,' and four[0] == 'f,,'):
        note = make_ups(note_counter)
        if(note == "c'"):
          return note
        else:
          return "<c' e' g'>4"
      elif (one[0] == 'c ' and two[0] == "g'" and three[0] == 'e ' and one[1] > 0.05):
        return "<c  e  g >4"
      elif (one[0] == 'a,,' and two[0] == 'e ' and three[0] == 'a,' and 75 < freq < 80):
        return "<a, c  e >4"
      elif (one[0] == "e'" and two[0] == 'a,' and three[0] == 'c,' and 54 < freq < 60 or 110 < freq < 115):
        return "<c' e' g'>4"
      elif (one[0] == "e'" and two[0] == 'a,' and three[0] == 'c,' and 60 < freq < 65):
        return "<a  c' e'>4"
      elif (one[0] == "f'" and two[0] == 'f,' and three[0] == 'as,' and 60 < freq < 65 or 92 < freq <98):
        return "<f  a  c'>4"
      elif (one[0] == "d'" and two[0] == 'g,' and three[0] == 'g,,' and 81 < freq < 86):
        return "<g  b  d'>4"
      elif (one[0] == "a'" and two[0] == 'd ' and three[0] == 'f,' and 60 < freq < 65):
        return "<d' f' a'>4"
      elif (one[0] == "a'" and two[0] == 'd,' and three[0] == 'a ' and four[0] == 'd '):
        note = make_ups(note_counter)
        if(note == 'a '):
          return note
        else:
          return "<a c' e'>4"
      elif (one[0] == "f'" and two[0] == 'as,,' and three[0] == 'as,' and four[0] == 'f '):
        note = make_ups(note_counter)
        if(note == 'f '):
          return note
        else:
          return "<f a c'>4"
      elif (one[0] == "g'" and two[0] == 'c ' and three[0] == 'c,' and four[0] == 'ds,'):
        #return "<g b d'>8."
        note = make_ups(note_counter)
        if(note == 'g '):
          return note
        else:
          return "<g b d'>4"
      else:
        return make_ups(note_counter)

    else:
      return make_ups(note_counter)
    #old
    # return note_counter.most_common(1)[0][0]


# If f is within tolerance of a note (measured in cents - 1/100th of a semitone)
# return that note, otherwise returns None
# We scale to the 440 octave to check
def get_note_for_freq(f, tolerance=33):
    # Calculate the range for each note
    tolerance_multiplier = 2 ** (tolerance / 1200)
    #TODO -- need to expand on this and make it suitable for other octaves C1-C7
    note_ranges = {
        k: (v / tolerance_multiplier, v * tolerance_multiplier) for (k, v) in NOTES.items()
    }
    # Correcting C4
    note_ranges["c'"] = (255.1234455454, 265.999999798)
    # Get last and first freqs
    range_min = note_ranges["a,,,"][0]
    range_max = note_ranges["b''''"][1]
    if f < range_min:
        while f < range_min:
            f *= 2
    else:
        while f > range_max:
            f /= 2

    # Check if any notes match
    for (note, note_range) in note_ranges.items():
      #TODO--possibly have a check here where if f = 41.111111111111114, note = 'A0'
        if f > note_range[0] and f < note_range[1]:
            return note
    return None


# Assumes everything is either natural or sharp, no flats
# Returns the Levenshtein distance between the actual notes and the predicted notes
def calculate_distance(predicted, actual):
    # To make a simple string for distance calculations we make natural notes lower case
    # and sharp notes cap
    def transform(note):
        if "#" in note:
            return note[0].upper()
        return note.lower()

    return distance(
        "".join([transform(n) for n in predicted]), "".join([transform(n) for n in actual]),
    )