#
#Takes in a .wav file and interprets the notes that were played by using
# `note_recognition.py`.
#
import argparse
from imghdr import what
from pydub import AudioSegment
import pydub.scipy_effects
import numpy as np
import scipy
import matplotlib.pyplot as plt

from note_recognition_helper import (
    frequency_spectrum,
    calculate_distance,
    classify_note_attempt_1,
    classify_note_attempt_2,
    classify_note_attempt_3,
    leftAndRightHand,
)

#
#
# Returns perdicted starts in ms
def predict_note_starts(song, plot):
    #SHOULD BE LEN of 32
    # Size of segments to break song into for volume calculations
    SEGMENT_MS = 50
    # Minimum volume necessary to be considered a note
    VOLUME_THRESHOLD = -35
    # The increase from one sample to the next required to be considered a note
    EDGE_THRESHOLD = 3

    # Throw out any additional notes found in this window
    MIN_MS_BETWEEN = 100

    # Filter out lower frequencies to reduce noise
    song = song.high_pass_filter(80, order=4)
    # dBFS is decibels relative to the maximum possible loudness
    volume = [segment.dBFS for segment in song[::SEGMENT_MS]]

    predicted_starts = []
    for i in range(1, len(volume)):
        if volume[i] > VOLUME_THRESHOLD and volume[i] - volume[i - 1] > EDGE_THRESHOLD:
            ms = i * SEGMENT_MS
            # Ignore any too close together
            if len(predicted_starts) == 0 or ms - predicted_starts[-1] >= MIN_MS_BETWEEN:
                predicted_starts.append(ms)

    # Plot the volume over time (sec)
    if plot:
        x_axis = np.arange(len(volume)) * (SEGMENT_MS / 1000)
        plt.plot(x_axis, volume)

        # Add vertical lines for predicted note starts
        for ms in predicted_starts:
            plt.axvline(x=(ms / 1000), color="g", linewidth=0.5, linestyle=":")

        plt.show()

    return predicted_starts

#TODO - WHERE I NEED TO CHANGE FOR CHORDS
def predict_notes(song, starts, actual_notes, plot_fft_indices):
    predicted_notes = []
    #gets portions of the song and addresses each portion
    for i, start in enumerate(starts):
        sample_from = start + 50
        sample_to = start + 550
        if i < len(starts) - 1:
            sample_to = min(starts[i + 1], sample_to)
        segment = song[sample_from:sample_to]
        #TODO--might be where i need to make some changes
        # where I'll want to see how the segment is diff
        # for chords
        freqs, freq_magnitudes = frequency_spectrum(segment)

        predicted = classify_note_attempt_3(freqs, freq_magnitudes)

        predicted_notes.append(predicted or "U")

        # Print general info
        print("")
        print("Note: {}".format(i + 1))
        if i < len(actual_notes):
            print("Predicted: {} Actual: {}".format(predicted, actual_notes[i]))
        else:
            print("Predicted: {}".format(predicted))
        print("Predicted start: {}".format(start))
        length = sample_to - sample_from
        print("Sampled from {} to {} ({} ms)".format(sample_from, sample_to, length))
        print("Frequency sample period: {}hz".format(freqs[1]))

        # Print peak info
        peak_indicies, props = scipy.signal.find_peaks(freq_magnitudes, height=0.015)
        print("Peaks of more than 1.5 percent of total frequency contribution:")
        for j, peak in enumerate(peak_indicies):
            freq = freqs[peak]
            magnitude = props["peak_heights"][j]
            print("{:.1f}hz with magnitude {:.3f}".format(freq, magnitude))

        if i in plot_fft_indices:
            plt.plot(freqs, freq_magnitudes, "b")
            plt.xlabel("Freq (Hz)")
            plt.ylabel("|X(freq)|")
            plt.show()
    return predicted_notes

def createFreqArray(file):
    plot_starts = False
    note_file=None
    plot_fft_indices = []

    actual_notes = []
    if note_file:
        with open(note_file) as f:
            for line in f:
                actual_notes.append(line.strip())

    song = AudioSegment.from_file(file)
    song = song.high_pass_filter(80, order=4)

    starts = predict_note_starts(song, plot_starts)

    predicted_notes = predict_notes(song, starts, actual_notes, plot_fft_indices)

    #print("")
    #print("Predicted Notes")
    #print(predicted_notes)
    #print("Length: ", len(predicted_notes))

    #Splitting the returned notes into right and left hand
    # and have the position of when the note is supposed to get
    # played after each note.
    rh_notes, lh_notes = leftAndRightHand(predicted_notes)
    # whatWeAreReturningRh = " ".join(rh_notes)
    # whatWeAreReturningLh = " ".join(lh_notes)
    starts = [x - starts[0] for x in starts]
    return rh_notes, lh_notes, starts

if __name__ == "__main__":
    file = "TestRecording/C3gbC4d.wav"
    #WORKS for all notes these are all the files:
    # cMinorC4.wav don't work...,
    # "TestRecording/C3fac4.wav", "TestRecording/C4ceg.wav"
    # "TestRecording/C3gbC4d.wav", "TestRecording/C3aC4ce.wav"
    # "TestRecording/A0B0.wav", "TestRecording/notesC1.wav",
    # "TestRecording/notesC2.wav", "TestRecording/notesC3.wav",
    # "TestRecording/notesC4.wav", "TestRecording/notesC5.wav",
    # "TestRecording/notesC6.wav", "TestRecording/notesC7.wav",
    # "TestRecording/cMajor.wav"
    test = createFreqArray(file)
    print(test)