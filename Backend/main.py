#### Not currently working
## The idea is to get the bass clef working alongside default treble clef.
import abjad
from note_recognition import createFreqArray
import os
import re
#####
#Edited function from
#https://stackoverflow.com/questions/35091557/replace-nth-occurrence-of-substring-in-string#:~:text=def%20nth_replace%20%28string%2C%20old%2C%20new%2C%20n%3D1%2C%20option%3D%27only%20nth%27%29%3A,nth%20occurrence%20and%20all%20occurrences%20to%20the%20left.
def nth_replace(str,search,repl,index):
    split = str.split(search,index+1)
    # if(split[0] == split[1]):
    if(len(split)>1 and split[0] == split[1]):
        return str, 2
    if len(split)<=index+1:
        return str, 0
    return search.join(split[:-1])+repl+split[-1], 1
########################################################
def create(wav,Title,composer='Me',subtitle='Sub',tempo=None):
    titleString = " title = \markup {" + Title + "}"
    testString1 = """#(set-global-staff-size 14)
        \header {
            composer = \markup {""" + composer +"""}
            subtitle = \markup {""" + subtitle + """}
        """
    testString2 = """
        }
        \layout {
            indent = 0
        }
        """
    testString = testString1 + titleString + testString2
    # actualWav = 'Backend/Recordings/' + wav
    actualWav = 'Backend/Recordings/' + wav
    #Going to have to pass the .wav file into `createFreqArray`
    rh_notes, lh_notes, starts = createFreqArray(actualWav)
    #WORKS for all notes these are all the files:
        # "TestRecording/A0B0.wav", "TestRecording/notesC1.wav",
        # "TestRecording/notesC2.wav", "TestRecording/notesC3.wav",
        # "TestRecording/notesC4.wav", "TestRecording/notesC5.wav",
        # "TestRecording/notesC6.wav", "TestRecording/notesC7.wav",
        # "TestRecording/RH_and_LH.wav", "TestRecording/LH_and_RH_C2cC5cdC2a.wav"
        # chords
        # "TestRecording/C3fac4.wav", "TestRecording/C4ceg.wav"
        # "TestRecording/C3gbC4d.wav", "TestRecording/C3aC4ce.wav"
    rh_empty = 0
    lh_empty = 0
    if(not rh_notes):
        rh_empty = 1
    elif(not lh_notes):
        lh_empty = 1
    rh_played = ""
    lh_played = ""
    total_notes_added = 0
    rh_voice = None
    rh_staff = None
    lh_voice = None
    lh_staff = None
    rest = 0
    hand = ""
    rh_voice = abjad.Voice("", name="RH_Voice")
    rh_staff = abjad.Staff([rh_voice], name="RH_Staff")
    lh_voice = abjad.Voice("", name="LH_Voice")
    lh_staff = abjad.Staff([lh_voice], name="LH_Staff")
    while True:
        rh_played = ""
        lh_played = ""

        if((rh_empty == 0 and lh_empty == 0 and total_notes_added > int(rh_notes[-1]) and int(rh_notes[-1])>int(lh_notes[-1])) or
        (rh_empty == 0 and lh_empty == 0 and total_notes_added > int(lh_notes[-1]) and int(lh_notes[-1])>int(rh_notes[-1])) or
        (rh_empty == 1 and lh_empty == 0 and total_notes_added > lh_notes[-1]) or
        (lh_empty == 1 and rh_empty == 0 and total_notes_added > rh_notes[-1])):
            break
        if(total_notes_added in rh_notes):
            for (index, item) in enumerate(rh_notes):
                if item == total_notes_added:
                    rh_played = rh_notes[index-1] + " "
                    total_notes_added += 1
                    if(hand == "right"):
                        ## Probably a better way.  But this is what I came up with
                        if(rest == 3):
                            rh_voice.extend("r2 ")
                            rh_staff.extend([rh_voice])
                            rh_voice.extend("r4 ")
                            rh_staff.extend([rh_voice])
                        elif(rest == 2):
                            rh_voice.extend("r2 ")
                            rh_staff.extend([rh_voice])
                        else:
                            rh_voice.extend("r4 ")
                            rh_staff.extend([rh_voice])
                        hand = ""
                        rest = 0
                    rh_voice.extend(rh_played)
                    rh_staff.extend([rh_voice])
                    #empty lh but extend...
                    if(hand == ""):
                        hand = "left"
                        rest = 1
                    else:
                        rest = rest + 1
                        if(rest == 4):
                            lh_voice.extend("r1 ")
                            lh_staff.extend([lh_voice])
                            hand = ""
                            rest = 0
        elif(total_notes_added in lh_notes):
            for (index, item) in enumerate(lh_notes):
                if item == total_notes_added:
                    lh_played = lh_notes[index-1] + " "
                    total_notes_added += 1
                    if(hand == "left"):
                        if(rest == 3):
                            lh_voice.extend("r2 ")
                            lh_staff.extend([lh_voice])
                            lh_voice.extend("r4 ")
                            lh_staff.extend([lh_voice])
                        elif(rest == 2):
                            lh_voice.extend("r2 ")
                            lh_staff.extend([lh_voice])
                        else:
                            lh_voice.extend("r4 ")
                            lh_staff.extend([lh_voice])
                        hand = ""
                        rest = 0
                    lh_voice.extend(lh_played)
                    lh_staff.extend([lh_voice])
                    #empty rh but extend...
                    if(hand == ""):
                        hand = "right"
                        rest = 1
                    else:
                        rest = rest + 1
                        if(rest == 4):
                            rh_voice.extend("r1 ")
                            rh_staff.extend([rh_voice])
                            hand = ""
                            rest = 0
        else:
            break
            ## Case for using both hands in one frame.
            ## would be something like this
            ## Same as end case but after it add the notes in both hands and continue.
            if(rest != 0):
                if(hand == "right"):
                    if(rest == 3):
                        rh_voice.extend("r2 ")
                        rh_staff.extend([rh_voice])
                        rh_voice.extend("r4 ")
                        rh_staff.extend([rh_voice])
                    elif(rest == 2):
                        rh_voice.extend("r2 ")
                        rh_staff.extend([rh_voice])
                    else:
                        rh_voice.extend("r4 ")
                        rh_staff.extend([rh_voice])
                else:
                    if(rest == 3):
                        lh_voice.extend("r2 ")
                        lh_staff.extend([lh_voice])
                        lh_voice.extend("r4 ")
                        lh_staff.extend([lh_voice])
                    elif(rest == 2):
                        lh_voice.extend("r2 ")
                        lh_staff.extend([lh_voice])
                    else:
                        lh_voice.extend("r4 ")
                        lh_staff.extend([lh_voice])
    if(rest != 0):
        if(hand == "right"):
            if(rest == 3):
                rh_voice.extend("r2 ")
                rh_staff.extend([rh_voice])
                rh_voice.extend("r4 ")
                rh_staff.extend([rh_voice])
            elif(rest == 2):
                rh_voice.extend("r2 ")
                rh_staff.extend([rh_voice])
            else:
                rh_voice.extend("r4 ")
                rh_staff.extend([rh_voice])
        else:
            if(rest == 3):
                lh_voice.extend("r2 ")
                lh_staff.extend([lh_voice])
                lh_voice.extend("r4 ")
                lh_staff.extend([lh_voice])
            elif(rest == 2):
                lh_voice.extend("r2 ")
                lh_staff.extend([lh_voice])
            else:
                lh_voice.extend("r4 ")
                lh_staff.extend([lh_voice])

    staff_group = abjad.StaffGroup(
        [rh_staff, lh_staff],
        lilypond_type="PianoStaff",
        name="Piano_Staff",
    )

    if(lh_staff != ""):
        leaf = abjad.select(staff_group["LH_Voice"]).leaf(0)
        clef = abjad.Clef("bass")
        abjad.attach(clef, leaf)

    score = abjad.Score([staff_group], name="Score")
    string = abjad.lilypond(score)

    n = 1
    m, a = nth_replace(string,"s","is",0)
    while True:
        m, a = nth_replace(m,"s","is", n)
        n = n + 1
        if(a == 0):
            break
    mstring = m
    mstring, a = nth_replace(mstring,"baisis","bass",0)
    ##########################################################
    ####### Should work for this program with current names
    # If they are changed this will need to change.
    if(tempo != None):
        temp = mstring[:175] + "\t\t\t\t\\tempo 4 = " + tempo + "\n"
        mstring = temp + mstring[175:]
    ##########################################################
    File = abjad.LilyPondFile([testString, mstring])
    title = re.sub(r'\s+', '', Title)
    returnTitle = '/' + title + '.pdf'
    abjad.persist.as_pdf(File, os.path.join(os.getcwd(), 'Backend\\pdfFiles\\' + title))
    #abjad.show(File)
    return returnTitle

if __name__ == '__main__':
    create("/rh_and_lh.wav","SP", tempo = '65')
    #tests:
    #"/marry_had_a_little_lamb.wav", "/Twinkle_both_hands.wav"
    #"/Twinkle_rh.wav", "/Twinkle_lh.wav", "/yankee_doodle.wav"
    #"/heart_and_soul_rh.wav", "/Jingle_bells.wav"
    #"/heart_and_soul_chords.wav",
    #doesn't work:
    #
