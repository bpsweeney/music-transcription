% #(set-global-staff-size 14)
        \header {
            composer = \markup { Me }
            subtitle = \markup { You }
         title = \markup {Twi}
        }
        \layout {
            indent = 0
        }

% \context Score = "Score"
<<
    \context PianoStaff = "Piano_Staff"
    <<
        \context Staff = "RH_Staff"
        {
            \context Voice = "RH_Voice"
            {
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
            }
        }
        \context Staff = "LH_Staff"
        {
            \context Voice = "LH_Voice"
            {
                \clef "bass"
                c4
                c4
                g4
                g4
                a4
                a4
                g4
                f4
                f4
                e,4
                e,4
                d4
                d4
                c4
                g4
                g4
                f4
                f4
                e,4
                d4
                g4
                g4
                f4
                f4
                e,4
                c4
                g4
                g4
                a4
                g4
            }
        }
    >>
>>
%! abjad.LilyPondFile._get_format_pieces()
\version "2.22.1"
%! abjad.LilyPondFile._get_format_pieces()
\language "english"