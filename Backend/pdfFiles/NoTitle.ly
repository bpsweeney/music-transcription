% #(set-global-staff-size 14)
        \header {
            composer = \markup { Me }
            subtitle = \markup { You }
         title = \markup {No Title}
        }
        \layout {
            indent = 0
        }

% \context Score = "Score"
<<
    \context PianoStaff = "Piano_Staff"
    <<
        \context Staff = "RH_Staff"
        {
            \context Voice = "RH_Voice"
            {
                dis'4
                c'4
                <c' e' g'>8.
                r4
                gis'4
                d'4
                g'4
                f'4
                gis'4
                c'4
                c'4
                <c' e' g'>8.
                r4
                d'4
                <g b d'>8.
                f'4
                gis'4
                c'4
            }
        }
        \context Staff = "LH_Staff"
        {
            \context Voice = "LH_Voice"
            {
                \clef "bass"
                r4
                r4
                r4
                g,,4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                r4
                g,,4
                r4
                r4
                r4
                r4
                r4
            }
        }
    >>
>>
%! abjad.LilyPondFile._get_format_pieces()
\version "2.22.1"
%! abjad.LilyPondFile._get_format_pieces()
\language "english"