# Music Transcription



## Getting started

Make sure to download lilypond from https://lilypond.org/download.html. It needs to be callable from the command line.

After that take the requirements.txt from the Backend folder and install it into a new virtual python environment.

With both of those steps complete, you should be able to run main.py in the Backend folder or AbjadTests.py in the Program2 folder.

All reading of wav files is currently done through the Recordings folder in the Backend.  So put wav files you want to test in that folder.

## Authors and acknowledgment
------------------

## Project status
The programs in this project need improvement.  If you want to edit and expand upon one feel free to.